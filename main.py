from flask import Flask
from flask_restx import Api, Resource

from controller.ContactController import ContactController, ContactPostController

app = Flask(__name__)
api = Api(app)

api.add_resource(ContactPostController, '/contact')
api.add_resource(ContactController, '/contact/<int:id>')

if __name__ == '__main__':
  app.run(port=8000, debug=True)
 