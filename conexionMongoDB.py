import pymongo
from pymongo import MongoClient

class ConexionMongoDB():
    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client['agenda']
        self.collection = self.db['contacto']
        self.collection = self.db['contador']
        self.counter()
        
    def counter(self):
        if self.db.contador.find_one() is None:
            self.db.contador.insert_one({'collection': 'contacto', 
                                        'id': 0})
    def updateId(self, contact):
        contact['_id'] = self.db.contador.find_and_modify(
                                query = {'collection': 'contacto'},
                                update = {'$inc': {'id': 1}},
                                fields = {'id': 1, '_id': 0},
                                new = True
                                )['id']
        return contact
    
    def create(self, contact):
        self.db.contacto.insert_one(self.updateId(contact))

    def read(self, id):
        return self.db.contacto.find_one({'_id': id})
    
    def update(self, id, request):
        self.db.contacto.update_one({'_id': id}, {'$set': request})
    
    def delete(self, id):
        self.db.contacto.delete_one({'_id': id})