from flask_restx import Resource
from flask import request
import json

from conexionMongoDB import ConexionMongoDB

bd = ConexionMongoDB()

class ContactController(Resource):
    def get(self, id):
        return bd.read(id), 200
    
    def put(self, id):
        bd.update(id, request.get_json())
        return f'Contact Updated - id:{id}', 200
    
    def delete(self, id):
        bd.delete(id)
        return f'Contact Deleted - id:{id}', 200

class ContactPostController(Resource):
    def post(self):
        bd.create(request.get_json())
        return 'Contact Created', 201